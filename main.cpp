#include <iostream>
#include "display.h"
#include "shader.h"
#include "mesh.h"

using namespace std;

int main()
{

    Display display(800,600,"My very first OpenGL Window");

    Vertex verticies[] = {Vertex(glm::vec3(-0.5, -0.5, 0)),
                          Vertex(glm::vec3(0, 0.5, 0)),
                          Vertex(glm::vec3(0.5, -0.5, 0))
                         };
    Mesh mesh(verticies, sizeof (verticies)/sizeof (verticies[0]));

    Shader shader("../res/basicShader");

    while(!display.isClosed())
    {
        display.Clear(0.5f,0.6f,0.1f,1.0f);
        shader.Bind();
        mesh.Draw();

        display.Update();
    }
    return 0;
}
