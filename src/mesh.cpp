#include "mesh.h"

Mesh::Mesh(Vertex * vertices, uint numVertces):
    mDrawCount(numVertces)
{
    glGenVertexArrays(1,&mVertexArrayObject);
    glBindVertexArray(mVertexArrayObject);
    glGenBuffers(NUM_BUFFERS,mVertexArrayBuffer);
    glBindBuffer(GL_ARRAY_BUFFER,mVertexArrayBuffer[POSITION_VB]);
    glBufferData(GL_ARRAY_BUFFER,numVertces * sizeof (vertices[0]),vertices,GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,nullptr);


    glBindVertexArray(0);
}

void Mesh::Draw()
{
    glBindVertexArray(mVertexArrayObject);

    glDrawArrays(GL_TRIANGLES,0,mDrawCount);
    glBindVertexArray(0);

}

Mesh::~Mesh()
{
    glDeleteVertexArrays(1,&mVertexArrayObject);
}

Vertex::Vertex(const glm::vec3 &pos)
{
    this->pos = pos;
}
