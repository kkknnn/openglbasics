#include "shader.h"

static void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string & errorMessage)
{
    GLint success = 0;
    GLchar error[1024] = {0};
    if(isProgram)
    {
        glGetProgramiv(shader,flag,&success);
    }
    else
    {
    glGetShaderiv(shader,flag,&success);
    }

    if(GL_FALSE == success)
    {
        if(isProgram)
        {
            glGetProgramInfoLog(shader, sizeof (error), NULL, error);
        }
        else
        {
            glGetShaderInfoLog(shader, sizeof (error), NULL, error);
        }

        std::cout << errorMessage <<": " << error << std::endl;
    }
}

static std::string LoadShader(const std::string fileName)
{
    std::ifstream file;
    file.open((fileName).c_str());
    std::string output;
    std::string line;
    if(file.is_open())
    {
        while (file.good())
        {
            getline(file,line);
            output.append(line+"\n");
        }
    }
    else
    {

    std::cout << "Unable to load a shader" << std::endl;

    }

    return output;

}

static GLuint CreateShader(const std::string & text, GLenum shaderType)
{

    GLuint shader = glCreateShader(shaderType);

    if(0 == shader)
    {
        std::cout << "Error: Shader creation failed" << std::endl;
    }
    const GLchar* shaderSourceSting[1];
    GLint shaderSourceLength[1];
    shaderSourceSting[0] = text.c_str();
    shaderSourceLength[0] = text.length();

    glShaderSource(shader,1, shaderSourceSting, shaderSourceLength);
    glCompileShader(shader);
    CheckShaderError(shader,GL_COMPILE_STATUS,false,"Error: Shader compilation failed ");


    return  shader;

}

Shader::Shader(const std::string &fileName)
{
    mProgram = glCreateProgram();
    mShaders[0] = CreateShader(LoadShader(fileName + ".vs"), GL_VERTEX_SHADER);
    mShaders[1] = CreateShader(LoadShader(fileName + ".fs"), GL_FRAGMENT_SHADER);

    for (uint i = 0; i < NUM_SHADERS; i++)
    {
        glAttachShader(mProgram,mShaders[i]);
    }

    glBindAttribLocation(mProgram, 0, "position");

    glLinkProgram(mProgram);
    CheckShaderError(mProgram,GL_LINK_STATUS,true,"Error: Program linking failed ");

    glValidateProgram(mProgram);
    CheckShaderError(mProgram,GL_VALIDATE_STATUS,true,"Error: Program validate failed ");



}

Shader::~Shader()
{
    for (uint i = 0; i < NUM_SHADERS; i++)
    {
        glDetachShader(mProgram, mShaders[i]);
        glDeleteShader(mShaders[i]);
    }
    glDeleteProgram(mProgram);
}

void Shader::Bind()
{

    glUseProgram(mProgram);



}
