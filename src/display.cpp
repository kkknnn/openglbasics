#include "display.h"

Display::Display(int width, int height, const std::string &title):
    mWindow(nullptr,SDL_DestroyWindow),
    mIsClosed(false)
{
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);


    mWindow.reset( SDL_CreateWindow(title.c_str(),SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,width,height, SDL_WINDOW_OPENGL));
    mGlContex = std::make_unique <SDL_GLContext>(SDL_GL_CreateContext(mWindow.get()));
    GLenum status = glewInit();
    if(GLEW_OK != status)
    {
        std::cout << "Problem with initializing glew" << std::endl;
    }

}

Display::~Display()
{
    SDL_GL_DeleteContext(mGlContex.get());
    mGlContex.reset();
    SDL_DestroyWindow(mWindow.get());
    SDL_Quit();
}

void Display::Clear(float red, float green, float blue, float alpha)
{
    glClearColor(red,green,blue, alpha);
    glClear(GL_COLOR_BUFFER_BIT);
}

void Display::Update()
{
    SDL_GL_SwapWindow(mWindow.get());
    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
        if(event.type == SDL_QUIT)
        {
            mIsClosed = true;
        }
    }
}

bool Display::isClosed()
{
    return mIsClosed;
}

