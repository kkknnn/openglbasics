#ifndef SHADER_H
#define SHADER_H
#include <iostream>
#include <fstream>
#include <glew.h>
#include <string>

class Shader
{
public:
    Shader(const std::string & fileName);
    virtual ~Shader();
    void Bind();
private:
    static const unsigned int NUM_SHADERS = 2;
    static const unsigned int NUM_UNIFORMS = 3;
    GLuint mProgram;
    GLuint mShaders[NUM_SHADERS];
    GLuint mUniforms[NUM_UNIFORMS];

};

#endif // SHADER_H
