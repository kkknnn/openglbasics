#ifndef DISPLAY_H
#define DISPLAY_H
#include <string>
#include <SDL.h>
#include <memory>
#include <glew.h>
#include <iostream>

class Display
{
public:
    Display(int width, int height, const std::string & title);
    virtual ~Display();
    void Clear(float red, float green, float blue, float alpha);
    void Update();
    bool isClosed();

private:
    std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> mWindow;
    std::unique_ptr<SDL_GLContext> mGlContex;
    bool mIsClosed;

};



#endif // DISPLAY_H
