#ifndef MESH_H
#define MESH_H

#include <glm/glm.hpp>
#include <glew.h>

class Vertex
{

public:

    Vertex(const glm::vec3 &pos);
protected:

private:

    glm::vec3 pos;

};



class Mesh
{
public:
    Mesh(Vertex * vertices, uint numVertces);
    void Draw();
    virtual ~Mesh();
private:
    enum
    {
      POSITION_VB,
      NUM_BUFFERS
    };


    GLuint mVertexArrayObject;
    GLuint mVertexArrayBuffer[NUM_BUFFERS];
    unsigned int mDrawCount;
};

#endif // MESH_H
