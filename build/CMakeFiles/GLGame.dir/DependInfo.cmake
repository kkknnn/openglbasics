# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/iks/OpenGl/GLGame/src/stb_image.c" "/home/iks/OpenGl/GLGame/build/CMakeFiles/GLGame.dir/src/stb_image.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/SDL2"
  "/usr/include/GL"
  ".././include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/iks/OpenGl/GLGame/main.cpp" "/home/iks/OpenGl/GLGame/build/CMakeFiles/GLGame.dir/main.cpp.o"
  "/home/iks/OpenGl/GLGame/src/display.cpp" "/home/iks/OpenGl/GLGame/build/CMakeFiles/GLGame.dir/src/display.cpp.o"
  "/home/iks/OpenGl/GLGame/src/mesh.cpp" "/home/iks/OpenGl/GLGame/build/CMakeFiles/GLGame.dir/src/mesh.cpp.o"
  "/home/iks/OpenGl/GLGame/src/shader.cpp" "/home/iks/OpenGl/GLGame/build/CMakeFiles/GLGame.dir/src/shader.cpp.o"
  "/home/iks/OpenGl/GLGame/src/texture.cpp" "/home/iks/OpenGl/GLGame/build/CMakeFiles/GLGame.dir/src/texture.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/SDL2"
  "/usr/include/GL"
  ".././include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
